
class SwitchDict:
    @staticmethod
    def error_calc():
        print("Cannot calculate equation!")

    @staticmethod
    def error_input():
        print("Cannot calculate equation!")

    # @TODO Add code with dictionary implementation for basic +, -, *, / equations


if __name__ == "__main__":
    MyDict = SwitchDict()
    # Take input
    try:
        print("Please add simple equation (with whitespaces). eg. 3 * 5")
        line = input("Input: ")
        parts = line.split(" ")
        variable_1 = int(parts[0])
        operation = parts[1]
        variable_2 = int(parts[2])
    except:
        MyDict.error_input()

    # Calc result
    x = lambda a, b : a * b + a - b
    print(f"helper lambda result: {x(variable_1, variable_2)}")
    # @TODO Add code witch call dictionary with parameters
