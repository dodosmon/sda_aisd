class RecursionSequence:
    @staticmethod
    def sequence_sum(val):
        begin, end, step = val
        return val


if __name__ == "__main__":
    sda_rec = RecursionSequence()

    assert sda_rec.sequence_sum((2, 2, 2)) == 2, "sequence_sum is not working well yet!"
    assert sda_rec.sequence_sum((2, 6, 2)) == 12, "sequence_sum is not working well yet!"
    assert sda_rec.sequence_sum((1, 5, 1)) == 15, "sequence_sum is not working well yet!"
    assert sda_rec.sequence_sum((1, 5, 3)) == 5, "sequence_sum is not working well yet!"
    assert sda_rec.sequence_sum((6, 2, 3)) == 0, "sequence_sum is not working well yet!"
    print("looks like sequence_sum is working!")
